int green = 11;
int yellow = 10;
int red = 9;

int daySensor = 8;


int isDay;

void setup() {
  // put your setup code here, to run once:
  pinMode(green, OUTPUT);
  pinMode(yellow, OUTPUT);
  pinMode(red, OUTPUT);
  pinMode(daySensor, INPUT);
  
}

void dayMode()
{
  digitalWrite(red, HIGH);
  delay(5000);
  digitalWrite(yellow, HIGH);
  delay(2000);
  digitalWrite(red, LOW);
  digitalWrite(yellow, LOW);
  digitalWrite(green, HIGH);
  delay(5000);
  digitalWrite(green, LOW);
  digitalWrite(yellow,HIGH);
  delay(2000);
  digitalWrite(yellow,LOW);
}


void nightMode(int flashCount)
{  
  for (int i = 0; i < flashCount; i++)
  {
    digitalWrite(yellow, HIGH);
    delay(1000);
    digitalWrite(yellow,LOW);
    delay(1000);
  }
}


void loop() {
  // put your main code here, to run repeatedly:

  // [0] - ; [1] - 
  isDay = digitalRead(daySensor);

  if (isDay == 1)
  {
    dayMode();
  }
  else 
  {
    nightMode(5);
  }
  
  
 
}
